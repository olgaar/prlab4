package prlab4;

import javax.mail.MessagingException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class App extends JFrame {

    private App() {
        InitializeMainUI();
    }

    public static void main(String[] args) {
        App app = new App();
    }

    private void InitializeMainUI() {


        JFrame f = new JFrame("Email Client");
        JButton b1 = new JButton("Read email");
        JButton b2 = new JButton("Send email");
        b1.setBounds(50, 50, 140, 40);
        b2.setBounds(50, 150, 140, 40);
        f.add(b1);
        f.add(b2);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        MailSender mailSender = new MailSender();


        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                mailSender.openSender();
            }
        });


        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new MailReader();
            }
        });
    }
}
